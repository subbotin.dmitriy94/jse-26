package com.tsconsulting.dsubbotin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractEntity {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Override
    @NotNull
    public String toString() {
        return id;
    }

}