package com.tsconsulting.dsubbotin.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @NotNull
    private String userId = UUID.randomUUID().toString();

    @Override
    @NotNull
    public String toString() {
        return super.toString() + " - User Id: " + userId + "; ";
    }

}