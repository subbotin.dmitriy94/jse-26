package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Task not found!");
    }

}