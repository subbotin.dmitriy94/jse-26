package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-list";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project list.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter sort:");
        TerminalUtil.printMessage(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projects;
        try {
            @NotNull Sort sortType = EnumerationUtil.parseSort(sort);
            TerminalUtil.printMessage(sortType.getDisplayName());
            projects = serviceLocator.getProjectService().findAll(currentUserId, sortType.getComparator());
        } catch (UnknownSortException e) {
            projects = serviceLocator.getProjectService().findAll(currentUserId);
        }
        int index = 1;
        for (@NotNull final Project project : projects) TerminalUtil.printMessage(index++ + ". " + project);
    }

}
