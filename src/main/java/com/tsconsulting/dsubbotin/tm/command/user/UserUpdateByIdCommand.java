package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserUpdateByIdCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-update-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Update user by id.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String id = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getUserService().findById(id);
        TerminalUtil.printMessage("Enter last name:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter first name:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter middle name:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateById(id, lastName, firstName, middleName, email);
        TerminalUtil.printMessage("[User updated]");
    }

}
