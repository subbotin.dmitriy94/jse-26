package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-change-password";
    }

    @Override
    @NotNull
    public String description() {
        return "User change password.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String id = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter new password:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(id, newPassword);
        TerminalUtil.printMessage("Password changed.");
    }

}
