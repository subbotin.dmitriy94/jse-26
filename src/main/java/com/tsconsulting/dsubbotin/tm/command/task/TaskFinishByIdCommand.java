package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-finish-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Finish task by id.";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskService().finishById(currentUserId, id);
        TerminalUtil.printMessage("[Updated task status]");
    }

}
