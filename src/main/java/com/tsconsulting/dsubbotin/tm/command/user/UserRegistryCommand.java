package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-registry";
    }

    @Override
    @NotNull
    public String description() {
        return "User registry.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter password:");
        @NotNull final String password = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Enter email:");
        @NotNull final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        TerminalUtil.printMessage("[User registered]");
    }

}
