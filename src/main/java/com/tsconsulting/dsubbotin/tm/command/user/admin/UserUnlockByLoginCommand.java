package com.tsconsulting.dsubbotin.tm.command.user.admin;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class UserUnlockByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    @NotNull
    public String description() {
        return "User unlock by login.";
    }

    @Override
    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().unlockByLogin(login);
        TerminalUtil.printMessage(String.format("%s user unlocked.", login));
    }

}
