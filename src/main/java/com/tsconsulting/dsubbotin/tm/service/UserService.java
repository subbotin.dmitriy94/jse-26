package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Override
    public void create(@NotNull final String login, @NotNull final String password) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        userRepository.add(user);
    }

    @Override
    public void create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role
    ) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        userRepository.add(user);
    }

    @Override
    public void create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final Role role,
            @NotNull final String email
    ) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(getPasswordHash(password));
        user.setRole(role);
        user.setEmail(email);
        userRepository.add(user);
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        return userRepository.findByLogin(login);
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        userRepository.removeByLogin(login);
    }

    @Override
    public void setPassword(
            @NotNull final String id,
            @NotNull final String password
    ) throws AbstractException {
        checkId(id);
        checkPassword(password);
        @NotNull final User user = findById(id);
        user.setPasswordHash(getPasswordHash(password));
    }

    @Override
    public void setRole(@NotNull final String id, @NotNull final Role role) throws AbstractException {
        checkId(id);
        @NotNull final User user = findById(id);
        user.setRole(role);
    }

    @Override
    public void updateById(
            @NotNull final String id,
            @NotNull final String lastName,
            @NotNull final String firstName,
            @NotNull final String middleName,
            @NotNull final String email
    ) throws AbstractException {
        @NotNull final User user = findById(id);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
    }

    @Override
    public boolean isLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        return userRepository.isLogin(login);
    }

    @Override
    public void lockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockByLogin(@NotNull final String login) throws AbstractException {
        checkLogin(login);
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
    }

    private void checkLogin(@NotNull final String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(@NotNull final String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

    @NotNull
    private String getPasswordHash(@NotNull String password) {
        return HashUtil.salt(propertyService.getPasswordIteration(), propertyService.getPasswordSecret(), password);
    }

}