package com.tsconsulting.dsubbotin.tm.service;

import com.jcabi.manifests.Manifests;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT_VALUE = "1.26.2";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT_VALUE = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT_VALUE = "Dima Subbotin";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT_VALUE = "subbotin.dmitriy94@gmail.com";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue("buildVersion", APPLICATION_VERSION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValue("developer", DEVELOPER_NAME_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValue("email", DEVELOPER_EMAIL_DEFAULT_VALUE);
    }

    @Override
    public int getPasswordIteration() {
        return Integer.parseInt(getPropertyValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT_VALUE));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getPropertyValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    private String getPropertyValue(@NotNull final String property, @NotNull final String defaultValue) {
        if (System.getProperties().contains(property)) return System.getProperty(property);
        if (System.getenv().containsKey(property)) return System.getenv(property);
        return properties.getProperty(property, defaultValue);
    }

    @NotNull
    private String getValue(@NotNull final String entry, @NotNull final String defaultValue) {
        if (Manifests.exists(entry)) return Manifests.read(entry);
        return defaultValue;
    }

}
