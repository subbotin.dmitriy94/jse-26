package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import org.jetbrains.annotations.NotNull;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existById(@NotNull String userId, @NotNull String id) throws AbstractException;

    @NotNull
    Project findByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void removeByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void updateBuyId(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void updateByIndex(
            @NotNull String userId,
            int index,
            @NotNull String name,
            @NotNull String description
    ) throws AbstractException;

    void startById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void startByIndex(@NotNull String userId, int index) throws AbstractException;

    void startByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void finishById(@NotNull String userId, @NotNull String id) throws AbstractException;

    void finishByIndex(@NotNull String userId, int index) throws AbstractException;

    void finishByName(@NotNull String userId, @NotNull String name) throws AbstractException;

    void updateStatusById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByIndex(
            @NotNull String userId,
            int index,
            @NotNull Status status
    ) throws AbstractException;

    void updateStatusByName(
            @NotNull String userId,
            @NotNull String name,
            @NotNull Status status
    ) throws AbstractException;

}
