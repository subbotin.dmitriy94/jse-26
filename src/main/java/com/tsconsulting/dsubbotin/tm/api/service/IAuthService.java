package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthService {

    @NotNull
    String getCurrentUserId() throws AbstractException;

    void setCurrentUserId(@NotNull String id);

    void logout() throws AbstractException;

    void login(@NotNull String login, @NotNull String password) throws AbstractException;

    void registry(
            @NotNull String login,
            @NotNull String password,
            @NotNull String email
    ) throws AbstractException;

    @NotNull
    User getUser() throws AbstractException;

    void checkRoles(@Nullable Role... roles) throws AbstractException;

}
