package com.tsconsulting.dsubbotin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class SystemUtil {

    public long getPID() {
        @Nullable final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName == null || processName.isEmpty()) return 0;
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (@NotNull final Exception e) {
            return 0;
        }
    }

}
