package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasCreated;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public final class ComparatorByCreated implements Comparator<IHasCreated> {

    public static final ComparatorByCreated INSTANCE = new ComparatorByCreated();

    @Override
    public int compare(@NotNull final IHasCreated o1, @NotNull final IHasCreated o2) {
        return o1.getCreateDate().compareTo(o2.getCreateDate());
    }

}
