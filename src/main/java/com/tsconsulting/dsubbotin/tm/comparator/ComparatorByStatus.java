package com.tsconsulting.dsubbotin.tm.comparator;

import com.tsconsulting.dsubbotin.tm.api.entity.IHasStatus;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public final class ComparatorByStatus implements Comparator<IHasStatus> {

    public static final ComparatorByStatus INSTANCE = new ComparatorByStatus();

    @Override
    public int compare(@NotNull final IHasStatus o1, @NotNull final IHasStatus o2) {
        return o1.getStatus().getPriority() - o2.getStatus().getPriority();
    }

}
